### Meta:
Useful image with **CI/CD** **AWS**, for applications written in **NodeJs**

Created by: [https://devticon.com](https://devticon.com) 

### Includes:

* **alpine**: edge
* [nodejs](https://nodejs.org/en/): 12.18.2
* [npm](https://nodejs.org/en/): 6.14.5
* [yarn](https://yarnpkg.com/): 1.22.4
* [python](hhttps://www.python.org/): 3.8.3
* [pip](https://pypi.org/project/pip/): 3.8.3

* [awscli](https://aws.amazon.com/cli/): 1.18.97
* [serverless](https://www.npmjs.com/package/serverless): 1.74.1
* [aws-cdk](https://docs.aws.amazon.com/cdk/latest/guide/home.html): 1.51.0
* [terraform](https://www.terraform.io/): 0.12.28
* [pulumi](https://www.pulumi.com/): 2.6.1

#### Utils:
* curl
* unzip
* envsubst
* openssh-client
* get-cfn-output - example: get-cfn-param StackName.OutputName
* node-get-package-version - return version from package.json
