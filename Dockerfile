FROM alpine:edge

RUN apk --no-cache add nodejs=12.18.2-r0
RUN apk --no-cache add npm=12.18.2-r0
RUN apk --no-cache add curl
RUN apk --no-cache add unzip
RUN apk --no-cache add libc6-compat
RUN apk --no-cache add python3=3.8.3-r0
RUN apk --no-cache add py-pip

RUN apk add --no-cache python3 py-pip
RUN pip install awscli

RUN npm config set unsafe-perm true
RUN npm i yarn@1.22.4 -g
RUN npm i serverless@1.74.1 -g
RUN npm i aws-cdk@1.51.0 -g

RUN apk --no-cache add openssh-client
RUN apk --no-cache add gettext

COPY utils/node-get-package-version.sh /usr/local/bin/node-get-package-version
COPY utils/get-cfn-output.js /usr/local/bin/get-cfn-output
RUN chmod +x /usr/local/bin/node-get-package-version
RUN chmod +x /usr/local/bin/get-cfn-output

RUN curl https://releases.hashicorp.com/terraform/0.12.28/terraform_0.12.28_linux_amd64.zip  --output terraform.zip
RUN unzip terraform.zip
RUN mv ./terraform /usr/local/bin
RUN rm -rf terraform.zip

RUN apk --no-cache add terraform
RUN curl -fsSL https://get.pulumi.com | sh

ENV PATH /usr/local/aws-cli/v2/bin:/root/.pulumi/bin:$PATH
