#!/usr/bin/node
const { exec } = require('child_process');
const [stack, output] = process.argv[2].split('.');

const command = `
aws cloudformation describe-stacks \
 --stack-name "${stack}" \
 --query "Stacks[0].Outputs[?OutputKey=='${output}'].OutputValue" \
 --output text
`;

exec(command, (err, stdout, stderr) => {
  if (err) {
    console.error(err.message)
  } else {
    console.log(stdout);
  }
});
